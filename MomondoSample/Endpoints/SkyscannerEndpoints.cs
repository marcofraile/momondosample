﻿using MomondoSample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample
{
    public class BrowseDates
    {
        SkyscannerCall sc;
        

        public BrowseDates()
        {
            sc = new SkyscannerCall();
        }

        public BrowseDatesResponse GetDates(string origin, string destination, string outbounddate, string inbounddate, string forwardedfor, string currency, string market, string locale)
        {
            string url = String.Format("http://partners.api.skyscanner.net/apiservices/browsedates/v1.0/{0}/{1}/{2}/{3}/{4}/{5}/{6}", market, currency, locale, origin, destination, outbounddate, inbounddate);

            var result = sc.Get(url, forwardedfor);

            var response = JsonConvert.DeserializeObject<BrowseDatesResponse>(result);

            return response;
        }
    }
}
