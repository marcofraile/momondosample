﻿using MomondoSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample.Managers
{
    public class FlightFinder
    {
        private List<Quote> returnFlights;
        private List<Quote> onewayOut;
        private List<Quote> onewayIn;
        private List<Quote> cheapestFlights;

        private string origin;
        private string destination;
        private string currency;
        private string locale;
        private string market;


        public FlightFinder(string origin, string destination, string currency, string locale, string market)
        {
            returnFlights = new List<Quote>();
            onewayOut = new List<Quote>();
            onewayIn = new List<Quote>();
            cheapestFlights = new List<Quote>();

            this.origin = origin;
            this.destination = destination;
            this.currency = currency;
            this.locale = locale;
            this.market = market;
        }

        /// <summary>
        /// Method finds the cheapest combinations of flights based on desired 
        /// trip length and travel dates using cached flight prices using the Skyscanner api.
        /// Find all cached partial flight results for each month. Finds the best combination 
        /// one way departures and arrival flights using the FlightHandler class.
        /// </summary>
        /// <param name="numberOfResults">Number of total results returned</param>
        /// <param name="minNumberOfDays">Mininum number of days for travel</param>
        /// <param name="maxNumberOfDays">Maxium number of days for travel</param>
        /// <param name="forwardedFor">IP address of the user</param>
        /// <param name="months">The possible travel months</param>
        /// <returns>A list of the cheapest return flights found that matches the search criterias</returns>
        public List<Quote> GetCheapestFlights(int numberOfResults, int minNumberOfDays, int maxNumberOfDays, string forwardedFor, string[] months)
        {
            BrowseDates browseDates = new BrowseDates();

            foreach (var month in months)
            {
                //find all results for each month
                BrowseDatesResponse partialResult = browseDates.GetDates(origin, destination, month, month, forwardedFor, currency, market, locale);

                //1. find flights where there is both outbound and inbound quotes
                //2. check if the flights are compatible with the search criterias
                //3. find oneway outbound flights 
                //4. find oneway inbound flights                    
                if (partialResult.Quotes != null)
                {                    
                    var partialReturnFlights = partialResult.Quotes.Where(q => q.OutboundLeg != null && q.InboundLeg != null);
                    
                    partialReturnFlights = returnFlights.Where(x => isCompatible(x, minNumberOfDays, maxNumberOfDays));               
                    returnFlights.AddRange(partialReturnFlights);

                    var partialOnewayOutboundFlights = partialResult.Quotes.Where(q => q.OutboundLeg != null && q.InboundLeg == null);                    
                    onewayOut.AddRange(partialOnewayOutboundFlights);

                    var partialOnewayInboundFlights = partialResult.Quotes.Where(q => q.OutboundLeg == null && q.InboundLeg != null);                   
                    onewayIn.AddRange(partialOnewayInboundFlights);
                }
            }

            FlightHandler flightHandler = new FlightHandler();
            List<Quote> cheapestCombinations = flightHandler.FindCheapestCombination(onewayOut, onewayIn, minNumberOfDays, maxNumberOfDays, numberOfResults);
            
            if (cheapestCombinations != null)
            {
                cheapestFlights.AddRange(cheapestCombinations);
            }

            if (returnFlights != null)
            {
                cheapestFlights.AddRange(returnFlights.OrderBy(x => x.MinPrice));
            }

            cheapestFlights = cheapestFlights.OrderBy(x => x.MinPrice).Take(numberOfResults).ToList();

            return cheapestFlights;       
        }

        public bool isCompatible(Quote quote, int minDays, int maxDays)
        {
            DateTime outboundDate = DateTime.Parse(quote.OutboundLeg.DepartureDate);
            DateTime inboundDate = DateTime.Parse(quote.InboundLeg.DepartureDate);

            if (outboundDate != null && inboundDate != null)
            {
                if (outboundDate < inboundDate && (inboundDate.Subtract(outboundDate).Days > minDays && inboundDate.Subtract(outboundDate).Days < maxDays))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
    }
}
