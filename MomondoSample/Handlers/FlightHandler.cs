﻿using C5;
using MomondoSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample
{
    public class FlightHandler
    {
        private int minDays;
        private int maxDays;

        /// <summary>
        /// Takes two collections of sorted inbound and outbound flights. 
        /// Creates a heap and puts the best combination(best departure and best return) 
        /// with their corresponding indices in their collection into the heap. 
        /// Pop the best element froom heap (sorted by total price) and add to visited, 
        /// check if the departure and return are compatible and fits the search criteria. 
        /// Add to results if it passes.
        /// Repeat until there is enough combinations by putting adjacent combinations 
        /// (second best departure and return) into the heap.
        /// </summary>
        /// <param name="outboundFlights">departure flights sorted by price</param>
        /// <param name="inboundFlights">departyre flights sorted by price</param>
        /// <param name="minDays">The minimum number of days between departure and return</param>
        /// <param name="maxDays">The maximum number of days between departure and return</param>
        /// <param name="numberOfResults">The number of results</param>
        /// <returns></returns>
        public List<Quote> FindCheapestCombination(IEnumerable<Quote> outboundFlights, IEnumerable<Quote> inboundFlights, int minDays, int maxDays, int numberOfResults)
        {
            this.minDays = minDays;
            this.maxDays = maxDays;

            if (outboundFlights.Count() == 0 && inboundFlights.Count() == 0)
                return null;

            var sortedOutbound = outboundFlights.OrderBy(x => x.MinPrice).ToArray();
            var sortedInbound = inboundFlights.OrderBy(x => x.MinPrice).ToArray();

            var handle = new FlightComparerHandle();
            handle.Price = sortedOutbound[0].MinPrice + sortedInbound[0].MinPrice;
            handle.OutboundId = 0;
            handle.InboundId = 0;

            IntervalHeap<FlightComparerHandle> heap = new IntervalHeap<FlightComparerHandle>(new FlightQuoteComparer());
            heap.Add(handle);

            List<FlightComparerHandle> results = new List<FlightComparerHandle>();
            C5.HashSet<FlightComparerHandle> visited = new C5.HashSet<FlightComparerHandle>();

            while (!heap.IsEmpty && results.Count < numberOfResults)
            {
                var cc = heap.DeleteMin();
                if (visited.Contains(cc))
                    continue;

                visited.Add(cc);

                //if compatible (outbound is before inbound) add to results
                if (isCompatible(sortedOutbound[cc.OutboundId], sortedInbound[cc.InboundId]))
                    results.Add(cc);

                if (cc.OutboundId < sortedOutbound.Length - 1)
                    heap.Add(new FlightComparerHandle(sortedOutbound[cc.OutboundId + 1].MinPrice + sortedInbound[cc.InboundId].MinPrice, cc.OutboundId + 1, cc.InboundId));
                if (cc.InboundId < sortedInbound.Length - 1)
                    heap.Add(new FlightComparerHandle(sortedOutbound[cc.OutboundId].MinPrice + sortedInbound[cc.InboundId + 1].MinPrice, cc.OutboundId, cc.InboundId + 1));
            }

            //construct list of cheapest valid quotes
            List<Quote> cheapestCombinations = results.
                OrderBy(x => x.Price).
                Take(numberOfResults).
                Select(x => new Quote() {
                    MinPrice = x.Price,
                    OutboundLeg = sortedOutbound[x.OutboundId].OutboundLeg,
                    InboundLeg = sortedInbound[x.InboundId].InboundLeg
                }).ToList();

            if (cheapestCombinations.Count > 0)
            {
                return cheapestCombinations;
            }
            else
            {
                return null;
            }
        }

        public bool isCompatible(Quote outbound, Quote inbound)
        {
            DateTime outboundDate = DateTime.Parse(outbound.OutboundLeg.DepartureDate);
            DateTime inboundDate = DateTime.Parse(inbound.InboundLeg.DepartureDate);

            //check if outbound is before inbound number of days is between min and max
            if (outboundDate != null && inboundDate != null)
            {
                if (outboundDate < inboundDate && (inboundDate.Subtract(outboundDate).Days > minDays && inboundDate.Subtract(outboundDate).Days < maxDays))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
    }
    
    public class FlightComparerHandle
    {
        public FlightComparerHandle() { }
        public FlightComparerHandle(double Price, int outboundId, int inboundId)
        {
            this.Price = Price;
            this.OutboundId = outboundId;
            this.InboundId = inboundId;
        }

        public double Price { get; set; }
        public int OutboundId { get; set; }
        public int InboundId { get; set; }
    }

    public class FlightQuoteComparer : IComparer<FlightComparerHandle>
    {
        public int Compare(FlightComparerHandle a, FlightComparerHandle b)
        {
            if ((a.Price >= b.Price))
                return 0;
            if ((a.Price < b.Price))
                return -1;

            return 1;
        }
    }
}
