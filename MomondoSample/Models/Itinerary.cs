﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample.Models
{
    public class Itinerary
    {
        public double Price { get; set; }
        public Leg OutboundLeg { get; set; }
        public Leg InboundLeg { get; set; }
        public string Origin { get; set; }
        public string OriginIATA { get; set; }
        public string Destination { get; set; }
        public string DestinationIATA { get; set; }
        public string DepartureDate { get; set; }
        public string ReturnDate { get; set; }
        public string Currency { get; set; }
        public string RefUrl { get; set; }
        public string DepartureDay { get; set; }
        public string ReturnDay { get; set; }
        public string ShortDepartureDay { get; set; }
        public string ShortReturnDay { get; set; }
        public string Month { get; set; }
        public string ShortMonth { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public int Days { get; set; }
    }
}
