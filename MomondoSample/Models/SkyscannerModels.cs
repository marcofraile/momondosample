﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample.Models
{
    public class BrowseDatesResponse
    {
        public List<Currency> Currencies { get; set; }
        public Dates Dates { get; set; }
        public List<Quote> Quotes { get; set; }
        public List<Place> Places { get; set; }
        public List<Carrier> Carriers { get; set; }
    }

    public class Quote
    {
        public int QuoteId { get; set; }
        public double MinPrice { get; set; }
        public bool Direct { get; set; }

        public Leg OutboundLeg { get; set; }
        public Leg InboundLeg { get; set; }
    }

    public class Leg
    {
        public int[] CarrierIds { get; set; }
        public int OriginId { get; set; }
        public int DestinationId { get; set; }
        public String DepartureDate { get; set; }

    }

    public class Dates
    {
        public List<Date> OutboundDates { get; set; }
        public List<Date> InboundDates { get; set; }
    }

    public class Place
    {
        public int PlaceID { get; set; }
        public string IataCode { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }

    public class Carrier
    {
        public int CarrierID { get; set; }
        public string Name { get; set; }
    }

    public class Date
    {
        public string PartialDate { get; set; }
        public int[] QuoteIds { get; set; }
        public double Price { get; set; }
    }

    public class Currency
    {
        public string Code { get; set; }
        public string Symbol { get; set; }
        public string ThousandsSeparator { get; set; }
        public string DecimalSeparator { get; set; }
        public bool SymbolOnLeft { get; set; }
        public bool SpaceBetweenAmountAndSymbol { get; set; }
        public int RoundingCoefficient { get; set; }
        public int DecimalDigits { get; set; }
    }
}
