﻿using MomondoSample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MomondoSample
{
    public class SkyscannerCall
    {
        private static string apiKey = "xxxxx-xxxxx-xxxxx-xxxxx";

        public string Get(string url, string forwardedfor)
        {
            if (forwardedfor == null)
                return null;

            url = url + "?apiKey=" + apiKey;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-Forwarded-For", forwardedfor);

            var rslt = client.GetAsync(url).Result;
            var result = rslt.Content.ReadAsStringAsync().Result;
            return result;
        }
    }
}
