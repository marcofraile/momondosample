# README #

This code sample is an excerpt from a travel application that let's users find a set of flight itineraries based on duration of travel and month span. 

It is based on the Skyscanner API and shows the setup of calling the API, serializing the response and using a proprietary algorithm to find the cheapest combinations of departure and return flights from a large array of possible flights.

The main entry point in this example is the FlightManager class, which is responsible for calling the Skyscanner API based on the search criteria and passing the response into the FlightHandler which uses a priority queue to check all flight combinations efficiently and return the N cheapest results.

The code is developed by Marco Pock-Steen Fraile, and questions can be directed to marco.ps@gmail.com